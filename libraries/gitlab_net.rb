require 'socket'
require 'timeout'

module Gitlab
  module Common
    module Net
      def self.port_is_open?(ip, port)
        begin
          Timeout::timeout(1) do
            begin
              s = TCPSocket.new(ip, port)
              s.close
              Chef::Log.debug("Host is reachable: #{ip} on port #{port}")
              return true
            rescue Errno::ECONNREFUSED, Errno::EHOSTUNREACH
              raise "Host is not reachable: #{ip} on port #{port}"
            end
          end
        rescue Timeout::Error
          raise "Host timed out: #{ip} on port #{port}"
        end
      end
    end
  end
end
