# gitlab-common
This is a collection of recipes which are common across our fleet.

## Attributes

Default packages to install:

```json
'gitlab-common':{
  'default-packages':[
    'htop',
    'iotop',
    ...
  ]
}
```
This is an array of packages which can be overwritten in any precidence level.

OS level users to remove:

```json
'gitlab-common':{
  'remove-os-users':[
    'terraform',
    'franz',
    ...
  ]
}
```
An array of users which will be removed **IF** their userid is smaller than 500 (iE non-system users)

## Usage

### gitlab-common::default-packages

Include this in your run list to install the packages configured in the above mentioned attribute.

### gitlab-common::remove-os-users

Include this in your run list to remove users which have been configured in the above mentioned attribute.
