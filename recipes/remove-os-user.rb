node['gitlab-common']['remove-os-users'].uniq.each do |user_to_remove|
  cmd = Mixlib::ShellOut.new("id -u #{user_to_remove}")
  cmd.run_command
  user user_to_remove do
    action :remove
    not_if { cmd.stdout.to_i < 500 }
    ignore_failure true
  end
end unless node['gitlab-common']['remove-os-users'].empty?
