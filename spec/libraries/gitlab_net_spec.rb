require "spec_helper"
require "gitlab_net"

describe Gitlab::Common::Net do
  it "fails when a port is closed" do
    expect{
      Gitlab::Common::Net.port_is_open?("127.0.0.1", 2)
    }.to raise_error.with_message(/Host is not reachable.*/)
  end
end
