require_relative "../spec_helper"

describe "gitlab-common::default-packages" do
  let(:chef_run) {
    ChefSpec::SoloRunner.new {}.converge(described_recipe)
  }
  let(:node) { chef_run.node }

  default_packages=['kmod', 'curl', 'iotop', 'htop', 'pciutils', 'traceroute', 'acpid', 'unzip', 'telnet', 'ethtool', 'iptraf-ng', 'nmap', 'screen', 'haveged', 'bwm-ng', 'lsof']
  it "installs defaults" do
    expect(chef_run).to install_package('gitlab-common packages').with(package_name: default_packages) 
  end
end
