default['gitlab-common']['default-packages'] = [
  "kmod",
  "curl",
  "iotop",
  "htop",
  "pciutils",
  "traceroute",
  "acpid",
  "unzip",
  "telnet",
  "ethtool",
  "iptraf-ng",
  "nmap",
  "screen",
  "haveged",
  "bwm-ng",
  "lsof"
]
default['gitlab-common']['remove-os-users'] = ['terraform']
